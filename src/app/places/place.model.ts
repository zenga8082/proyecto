export interface Place {
    id: string;
    title: string;
    imageURL: string;
    imageURL2: string;
    imageURL3: string;
    dato: string,
    anfitrion: string,
    price: string;
    descripcion: string;
    comments: string[]
}