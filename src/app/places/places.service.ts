import { Injectable } from "@angular/core";
import { Place } from "../places/place.model";

@Injectable({
  providedIn: "root"
})
export class PlacesService {
  private places: Place[] = [
    {
      id: '1',
      title: "Apartamento",
      imageURL:
        "/assets/Apartamento/1.png",
      imageURL2:
        "/assets/Apartamento/2.png",
      imageURL3:
        "/assets/Apartamento/3.png",
      dato: "Apartamento Entero",
      anfitrion: "Carlos",
      price: "12,967",
      descripcion: "2 huéspedes 1 habitación 1 cama 1 baño", 
      comments: ["Dispondrás de toda la vivienda (apartamento) para ti.", "Este anfitrión se ha comprometido a seguir el proceso de limpieza avanzada en 5 pasos de Airbnb.", "Los Superhosts son anfitriones con experiencia y valoraciones excelentes que se esfuerzan para que la estancia de sus huéspedes sea siempre perfecta.", "Agrega las fechas de tu viaje para obtener los detalles de cancelación de esta estadía.", "Este alojamiento no es adecuado para niños menores de 12 años y el anfitrión no admite mascotas ni permite que se fume o que se celebren fiestas. "]
    },
    {
      id: '2',
      title: "Rustic Loft",
      imageURL:
        "/assets/Loft/1.png",
      imageURL2:
        "/assets/Loft/2.png",
      imageURL3:
        "/assets/Loft/3.png",
      dato: "Loft Entero",
      anfitrion: "Pablo",
      price: "12,154", 
      descripcion: "2 Huéspedes 1 Habitación 1 Cama 1 Baño",
      comments: ["Dispondrás de toda la vivienda (loft) para ti.", "Este anfitrión se ha comprometido a seguir el proceso de limpieza avanzada en 5 pasos de Airbnb.", "Un 100 % de los huéspedes más recientes ha valorado a Pablo con 5 estrellas en comunicación.", "Agrega las fechas de tu viaje para obtener los detalles de cancelación de esta estadía.", "Este alojamiento no es adecuado para bebés (de 0 a 2 años) y el anfitrión no admite mascotas ni permite que se fume o que se celebren fiestas."]
    },
    {
      id: '3',
      title: "Vintage City Lights",
      imageURL:
        "/assets/City/1.png",
      imageURL2:
        "/assets/City/2.png",
      imageURL3:
        "/assets/City/3.png",
      dato: "Apartamento Entero",
      anfitrion: "Eduardo",
      price: "21,271", 
      descripcion: "3 Huéspedes Estudio 1 Cama 1 Baño",
      comments: ["Dispondrás de toda la vivienda (apartamento) para ti.", "Este anfitrión se ha comprometido a seguir el proceso de limpieza avanzada en 5 pasos de Airbnb.", "El 100 % de los últimos huéspedes han valorado con 5 estrellas la ubicación.", "Agrega las fechas de tu viaje para obtener los detalles de cancelación de esta estadía.", "Este alojamiento no es adecuado para bebés (de 0 a 2 años) y el anfitrión no admite mascotas ni permite que se fume o que se celebren fiestas."]
    },
    {
      id: '4',
      title: "Rainforest Cabin",
      imageURL:
        "/assets/Cabin/1.png",
      imageURL2:
        "/assets/Cabin/2.png",
      imageURL3:
        "/assets/Cabin/3.png",
      dato: "Casa de huéspedes entera",
      anfitrion: "Alejandro",
      price: "17,624",
      descripcion: "4 Huéspedes 1 Habitación 2 Camas 1 Baño",
      comments: ["Dispondrás de toda la vivienda (casa de invitados) para ti.", "El 100 % de los últimos huéspedes han valorado con 5 estrellas la ubicación.", "El 100 % de los últimos huéspedes han valorado con 5 estrellas el proceso de llegada.", "Agrega las fechas de tu viaje para obtener los detalles de cancelación de esta estadía.", "Este alojamiento no es adecuado para bebés (de 0 a 2 años) y el anfitrión no permite que se celebren fiestas."]
    },
    {
      id: '5',
      title: "Apartamento Amueblado",
      imageURL:
        "/assets/Amueblado/1.png",
      imageURL2:
        "/assets/Amueblado/2.png",
      imageURL3:
        "/assets/Amueblado/3.png",
      dato: "Loft entero",
      anfitrion: "Pier Luigi",
      price: "30,387",
      descripcion: "2 huéspedes Estudio 1 cama 1 baño", 
      comments: ["Dispondrás de toda la vivienda (loft) para ti.", "Este anfitrión se ha comprometido a seguir el proceso de limpieza avanzada en 5 pasos de Airbnb.", "Accede al alojamiento con ayuda del portero.", "Agrega las fechas de tu viaje para obtener los detalles de cancelación de esta estadía.", "El anfitrión no admite mascotas ni permite celebrar fiestas ni fumar."]
    },
  ];

  constructor() {}

  getPlaces(): Place[] {
    return [...this.places];
  }

  getPlace(placeId: string) {
    return {
      ...this.places.find(place => {
        return place.id === placeId;
      })
    };
  }

  addPlace(title, imageURL, imageURL2, imageURL3, price, dato, anfitrion, descripcion) {
    this.places.push({
      title,
      imageURL,
      imageURL2,
      imageURL3,
      dato,
      anfitrion,
      price,
      descripcion,
      comments: [],
      id: this.places.length + 1 + ""
    });
  }

deletePlace(placeId: string) {
    this.places = this.places.filter(place => {
      return place.id !== placeId;
    });
  }

  getPlacesSearch(): Place[] {
    return [...this.places];
  }
}